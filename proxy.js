var globalTunnel = require('global-tunnel-ng');
globalTunnel.initialize({
    host: 'localhost',
    port: 8888,
    connect: 'neither',
    protocol: 'http:'
});