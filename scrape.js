require('dotenv').config()
const got = require('got')
const fsp = require('fs').promises;
// require('./proxy')

let userToken = ''
let errorNum = 0

const getNewToken = async () => {
    try {
        console.log('Fetching new token')
        const response = await got.post('https://api.nasapatrola.com/user/login', {
            json: true,
            body: {
                digitsUserId: process.env.DIGITS_USER_ID
            },
        })
        return response.body.token;
    } catch (error) {
        throw error
    }
}

const getPatrols = async () => {
    try {
        console.log('Fetching patrols')
        const response = await got('https://api.nasapatrola.com/Patrol/GetAll', {
            json: true,
            headers: {
                Authorization: userToken
            }
        })
        return response.body
    } catch (error) {
        throw error
    }
}

const getWeather = async () => {
    try {
        console.log('Fetching weather')
        const response = await got(`https://api.openweathermap.org/data/2.5/weather?id=${process.env.WEATHER_CITY_ID}&appid=${process.env.WEATHER_API_KEY}`, {
            json: true
        })
        return response.body
    } catch (error) {
        throw error
    }
}

const logToFile = async (patrols, weather) => {
    try {
        console.log('Writing to file')
        const data = JSON.stringify({ time: Date.now, weather, patrols })
        await fsp.appendFile(process.env.OUTPUT_FILE, `\n${data},`);
    } catch (error) {
        throw error;
    }
}

const logPatrols = async () => {
    try {
        console.log('Logging data')
        const [patrols, weather] = await Promise.all([
            getPatrols(),
            getWeather(),
        ]);
        await logToFile(patrols, weather)
        errorNum = 0
    } catch (error) {
        if(errorNum > 2){
            console.log('Too many requests are failing. Exiting.')
            process.exit()
        }
        errorNum++
        console.error('Error fetching patrols. Attempting to acquire new token: ' + error)
        userToken = await getNewToken() //handle promise
        logPatrols()
    }
}

(async () => {
    try {
        userToken = process.env.OVERRIDE_TOKEN || await getNewToken()
    } catch (error) {
        console.log("Unable to acquire token and no token provided. Exiting.")
        process.exit()
    }
    logPatrols()
    setInterval(logPatrols, 10 * 60 * 1000)
})();