# My Patrol Scrape

A small utility using the API in the MyPatrol app for scraping the data and storing it with additional metadata such as time and weather.  
The idea was to use the data to create a (naive) ML model to predict locations of patrols for a given time and weather.

**Requires:**  

- Node.js
- NPM/Yarn

## Install

```sh
yarn install
```

Set appropriate variables in the .env file. The service used for recording weather conditions is [Openweathermap](https://openweathermap.org/). 

## Usage

```sh
node scrape.js
```

The script will automatically reqacquire tokens, stop scraping when it detects too many errors or cannot acquire new token.